#! /usr/bin/env python

# version 0.1.1
# Inspired by https://github.com/luketimothyjones/pynetcheck

import argparse
import socket
import speedtest
from prometheus_client import CollectorRegistry, Gauge, Info, write_to_textfile


def populate_speedtest_registry(s_results, debug=False):
    """
        accept speedtest results and populate a prometheus data registry
    """

    # define the registry
    prom_registry = CollectorRegistry()

    my_hostname = socket.gethostname()

    try:
        isp_rating = float(s_results['client']['isprating'])
    except (TypeError, ValueError):
        isp_rating = 0.0

    # define gauges
    prom_gauges = [
        {
            'metric': 'speedtest_download_bits_second',
            'description': 'Speedtest Download Results',
            'value': s_results['download'],
        },
        {
            'metric': 'speedtest_upload_bits_second',
            'description': 'Speedtest Upload Results',
            'value': s_results['upload'],
        },
        {
            'metric': 'speedtest_received_bytes',
            'description': 'Speedtest Bytes Received',
            'value': s_results['bytes_received'],
        },
        {
            'metric': 'speedtest_sent_bytes',
            'description': 'Speedtest Bytes Sent',
            'value': s_results['bytes_sent'],
        },
        {
            'metric': 'speedtest_ping_seconds',
            'description': 'Speedtest Ping Results',
            'value': (s_results['ping'] / 1000),
        },
        {
            'metric': 'speedtest_server_latency_seconds',
            'description': 'Speedtest Server Latency',
            'value': (s_results['server']['latency']/ 1000),
        },
        {
            'metric': 'speedtest_server_distance_meters',
            'description': 'Distance to Server in Meters',
            'value': (s_results['server']['d'] * 1000),
        },
        {
            'metric': 'speedtest_isp_rating',
            'description': 'ISP Rating',
            'value': isp_rating,
        },
    ]

    # loop through gauges and set them
    for this_gauge in prom_gauges:
        prom_gauge = Gauge(
            this_gauge['metric'],
            this_gauge['description'],
            ["hostname", "sourceip", "isp"],
            registry=prom_registry,
        )
        prom_gauge.labels(
            my_hostname,
            s_results['client']['ip'],
            s_results['client']['isp'],
        ).set(this_gauge['value'])
        if debug:
            print(this_gauge['metric'], this_gauge['value'])


    # gather info
    prom_info_dict = {
        'server_country': s_results['server']['country'],
        'server_cc': s_results['server']['cc'],
        'timestamp': s_results['timestamp'],
        'server_host': s_results['server']['host'],
        'server_name': s_results['server']['name'],
        'server_sponsor': s_results['server']['sponsor'],
        'client_isp': s_results['client']['isp'],
        'client_ip': s_results['client']['ip'],
    }

    # share link
    if s_results['share']:
        prom_info_dict['share'] = s_results['share']

    # set info data
    prom_info = Info('speedtest_meta', 'Speedtest data', registry=prom_registry)
    prom_info.info(prom_info_dict)

    return prom_registry



def run_speedtest(debug=False):
    """ Run Speedtest and return dictionary """

    speed_test = speedtest.Speedtest()
    speed_test.get_best_server()
    speed_test.download()
    speed_test.upload()
    speed_test.results.share()
    return speed_test.results.dict()

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser()
    PARSER.add_argument(
        "-o",
        "--output",
        type=str,
        dest="outputfile",
        default="/usr/local/node_exporter/collections/speedtest.prom",
    )
    PARSER.add_argument(
        "-d",
        "--debug",
        action='store_true',
        default=False,
        help="Enable extra output for debugging",
    )
    ARGS = PARSER.parse_args()

    SPEEDTEST_RESULTS = run_speedtest(debug=ARGS.debug)

    REGISTRY = populate_speedtest_registry(SPEEDTEST_RESULTS, debug=ARGS.debug)

    write_to_textfile(ARGS.outputfile, REGISTRY)
