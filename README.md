# prometheus-speedtest

Tested in python 2.7 only.

Requires the following packages:

* prometheus-client>=0.4.1
* speedtest-cli>=2.0.2

Note, as of 10/10/2018, the version of prometheus-client on PyPi did not have all functionality used in this script, but you may install from Github:

```bash
pip install --upgrade https://github.com/prometheus/client_python/tarball/master
```

This will create a file for node_exporter to scrape, default location: `/usr/local/node_exporter/collections/speedtest.prom`, but this can be overridden with cli options.

```
./prometheus-speedtest.py -h
usage: prom-st.py [-h] [-o OUTPUTFILE] [-d]

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUTFILE, --output OUTPUTFILE
  -d, --debug           Enable extra output for debugging
```

Need something added?  Pull requests encouraged.
